#!/bin/bash

# H.A Daemon for jenkins

DAEMON_NAME="ha-daemon"
PID_FILE="$DAEMON_NAME.pid"
RUN_INTERVAL=10 #in seconds
SERVICE="jenkins"

myPid=$(echo $$)

function doCommands() {
    #Chek if master is down starting new master 
    masterStatus=$(curl --connect-timeout 5 -s -o /dev/null -w '%{http_code}' jenkins-host-1.external:8080)
    serviceStatus=$(systemctl is-active $SERVICE)
    if [[ $masterStatus -eq 000  && $serviceStatus == inactive ]]; then
        sudo systemctl start jenkins
    elif [[ $masterStatus -ne 000  && $serviceStatus == active ]]; then
        sudo systemctl stop jenkins
    fi
}

function checkDaemon() {
    if [ -z "$oldPid" ]; then
        return 0
    elif [[ $(ps aux | grep "$oldPid" | grep "$DAEMON_NAME" | grep -v grep) > /dev/null ]]; then
        if [ -f "$PID_FILE" ]; then
            if [[ $(cat "$PID_FILE") -eq "$oldPid" ]]; then
                # Daemon is running.
                return 1
            else
                # Daemon isn't running.
                return 0
            fi
        else
            #TODO check PID 
            return 1
        fi
        return 1
    fi
 }

function loop() {
	doCommands
	sleep $RUN_INTERVAL
	loop
}

function stopDaemon() {
    checkDaemon
    local daemonAlive=$?
    if [[ "$daemonAlive" = 0 ]]; then
        echo " * Error: $DAEMON_NAME is not running."
        exit 1
    fi
    echo " * Stopping $DAEMON_NAME"

    if [[ ! -z $(cat "$PID_FILE") ]]; then
        kill -9 $(cat "$PID_FILE") &> /dev/null
    fi
}

function startDaemon() {
    # Start the daemon.
    checkDaemon
    local daemonAlive=$?
    if [[ "$daemonAlive" = 1 ]]; then
        echo "Error: $DAEMON_NAME is already running."
        exit 1
    fi
    echo " * Starting $DAEMON_NAME with PID: $myPid."
    echo "$myPid" > "$PID_FILE"

    # Start the loop.
    loop
}

function statusDaemon() {
    checkDaemon
    local daemonAlive=$?
    if [[ "$daemonAlive" = 1 ]]
    then
        echo " * $DAEMON_NAME is running."
    else
        echo " * $DAEMON_NAME isn't running."
    fi
        exit 0
}

function restartDaemon() {
	checkDaemon
	local daemonAlive=$?
	if [[ "$daemonAlive" = 0 ]]; then
	    echo "$DAEMON_NAME isn't running."
	    exit 1
	fi
	stopDaemon
	startDaemon
}

#Commands input
if [ -f "$PID_FILE" ]; then
    oldPid=$(cat "$PID_FILE")
fi
checkDaemon
case "$1" in
start)
    startDaemon
;;
stop)
    stopDaemon
;;
status)
    statusDaemon
;;
restart)
    restartDaemon
;;
*)
echo "Error: usage $0 { start | stop | restart | status }"
exit 1
esac

exit 0
